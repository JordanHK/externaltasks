﻿using StreetHoop.Mappers;
using StreetHoops.DTO;
using StreetHoops.Models;
using StreetHoops.Repositories.Contracts;
using StreetHoops.Services.Contracts;

namespace StreetHoops.Services
{
    public class TeamsService : ITeamsService
    {
        private readonly ITeamsRepository teamsRepository;

        public TeamsService(ITeamsRepository teamsRepository)
        {
            this.teamsRepository = teamsRepository;
        }

        public async Task<ICollection<TeamDTO>> GetTopOffensive()
        {
            var teams = await this.teamsRepository.GetTopOffensive() ?? null;

            if (teams == null)
            {
                return null;
            }

            var DTOs = teams.Select(t => t.MapToDTO()).ToList();

            return DTOs;
        }

        public async Task<ICollection<TeamDTO>> GetTopDefensive()
        {
            var teams = await this.teamsRepository.GetTopDefensive() ?? null;

            if (teams == null)
            {
                return null;
            }

            var DTOs = teams.Select(t => t.MapToDTO()).ToList();

            return DTOs;
        }

        public async Task<Team> GetTeam(int id)
        {
            var team = await this.teamsRepository.GetTeam(id) ?? null;

            if (team == null)
            {
                return null;
            }

            return team;
        }

        public async Task<ICollection<TeamDTO>> GetAll()
        {
            var teams = await this.teamsRepository.GetAll() ?? null;

            if (teams == null)
            {
                return null;
            }

            var DTOs = teams.Select(t => t.MapToDTO()).ToList();

            return DTOs;
        }
    }
}