﻿using StreetHoop.Mappers;
using StreetHoops.DTO;
using StreetHoops.Models;
using StreetHoops.Repositories.Contracts;
using StreetHoops.Services.Contracts;

namespace StreetHoops.Services
{
    public class MatchUpsService : IMatchUpsService
    {
        private readonly IMatchUpsRepository matchUpsRepository;
        private readonly ITeamsService teamsService;

        public MatchUpsService(IMatchUpsRepository matchUpsRepository, ITeamsService teamsService)
        {
            this.matchUpsRepository = matchUpsRepository;
            this.teamsService = teamsService;
        }

        public async Task<ICollection<MatchUpDTO>> GetAll()
        {
            var matchUps = await this.matchUpsRepository.GetAll() ?? null;

            if (matchUps == null)
            {
                return null;
            }

            await this.MaterializeTeams(matchUps);

            var DTOs = matchUps.Select(m => m.MapToDTO()).ToList();

            return DTOs;
        }

        public async Task<ICollection<MatchUpDTO>> GetHighlight()
        {
            var matchUps = await this.matchUpsRepository.GetHighlight() ?? null;

            if (matchUps == null)
            {
                return null;
            }

            await this.MaterializeTeams(matchUps);

            var DTOs = matchUps.Select(m => m.MapToDTO()).ToList();

            return DTOs;
        }

        private async Task MaterializeTeams(ICollection<MatchUp> matchUps)
        {
            foreach (var matchUp in matchUps)
            {
                matchUp.HomeTeam = await this.teamsService.GetTeam(matchUp.HomeTeamId);
                matchUp.AwayTeam = await this.teamsService.GetTeam(matchUp.AwayTeamId);
            }
        }
    }
}