﻿using StreetHoops.DTO;

namespace StreetHoops.Services.Contracts
{
    public interface IMatchUpsService
    {
        public Task<ICollection<MatchUpDTO>> GetAll();

        public Task<ICollection<MatchUpDTO>> GetHighlight();
    }
}