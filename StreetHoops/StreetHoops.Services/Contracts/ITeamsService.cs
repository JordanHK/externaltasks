﻿using StreetHoops.DTO;
using StreetHoops.Models;

namespace StreetHoops.Services.Contracts
{
    public interface ITeamsService
    {
        public Task<ICollection<TeamDTO>> GetAll();

        public Task<ICollection<TeamDTO>> GetTopOffensive();

        public Task<ICollection<TeamDTO>> GetTopDefensive();

        public Task<Team> GetTeam(int id);
    }
}