﻿using StreetHoops.DTO;
using StreetHoops.Models;

namespace StreetHoop.Mappers
{
    public static class CustomMapper
    {
        public static MatchUpDTO MapToDTO(this MatchUp matchUp)
        {
            return new MatchUpDTO
            {
                HomeTeam = matchUp.HomeTeam.Name,
                AwayTeam = matchUp.AwayTeam.Name,
                HomeTeamScore = matchUp.HomeTeamScore,
                AwayTeamScore = matchUp.AwayTeamScore,
            };
        }

        public static TeamDTO MapToDTO(this Team team)
        {
            return new TeamDTO { Name = team.Name, Score = team.Score };
        }
    }
}