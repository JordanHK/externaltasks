﻿namespace StreetHoops.Models
{
    public class Team : EntityBase
    {
        public string Name { get; set; }
        public int? Score { get; set; }
        public virtual ICollection<MatchUp> HomeMatches { get; set; }
        public virtual ICollection<MatchUp> AwayMatches { get; set; }
    }
}