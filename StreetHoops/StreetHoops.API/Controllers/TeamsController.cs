﻿using Microsoft.AspNetCore.Mvc;
using StreetHoops.Services.Contracts;

namespace StreetHoops.Controllers
{
    [Route("api/teams")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService teamsService;

        private const string path_all = "all";
        private const string path_offensive = "topoffensive";
        private const string path_defensive = "topdefensive";

        public TeamsController(ITeamsService teamsService)
        {
            this.teamsService = teamsService;
        }

        [HttpGet(path_all)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var teams = await teamsService.GetAll();
                return this.Ok(teams);
            }
            catch (Exception e)
            {
                return this.StatusCode(500, value: e.Message);
            }
        }

        [HttpGet(path_offensive)]
        public async Task<IActionResult> GetTopOffensive()
        {
            try
            {
                var teams = await teamsService.GetTopOffensive();
                return this.Ok(teams);
            }
            catch (Exception e)
            {
                return this.StatusCode(500, value: e.Message);
            }
        }

        [HttpGet(path_defensive)]
        public async Task<IActionResult> GetTopDefensive()
        {
            try
            {
                var teams = await teamsService.GetTopDefensive();
                return this.Ok(teams);
            }
            catch (Exception e)
            {
                return this.StatusCode(500, value: e.Message);
            }
        }
    }
}