﻿using Microsoft.AspNetCore.Mvc;
using StreetHoops.Services.Contracts;

namespace StreetHoops.Controllers
{
    [Route("api/matches")]
    [ApiController]
    public class MatchesController : ControllerBase
    {
        private readonly IMatchUpsService matchUpsService;

        private const string path_all = "all";
        private const string path_highlight = "highlight";

        public MatchesController(IMatchUpsService matchUpsService)
        {
            this.matchUpsService = matchUpsService;
        }

        [HttpGet(path_all)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var allMatches = await matchUpsService.GetAll();
                return this.Ok(allMatches);
            }
            catch (Exception e)
            {
                return this.StatusCode(500, value: e.Message);
            }
        }

        [HttpGet(path_highlight)]
        public async Task<IActionResult> Get()
        {
            try
            {
                var matchUp = await matchUpsService.GetHighlight();
                return this.Ok(matchUp);
            }
            catch (Exception e)
            {
                return this.StatusCode(500, value: e.Message);
            }
        }
    }
}