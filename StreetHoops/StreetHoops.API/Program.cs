using Microsoft.EntityFrameworkCore;
using StreetHoops.API.Utils;
using StreetHoops.Data;
using StreetHoops.Repositories;
using StreetHoops.Repositories.Contracts;
using StreetHoops.Services;
using StreetHoops.Services.Contracts;

namespace StreetHoops
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers();

            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            //Configure DbContext
            builder.Services.AddScoped<StreetHoopsContext>();
            builder.Services.AddDbContext<StreetHoopsContext>(
                options => options.UseSqlServer(StreetHoopsConstants.sqlConnectionString)
            );

            //Configure Services
            builder.Services.AddScoped<IMatchUpsService, MatchUpsService>();
            builder.Services.AddScoped<ITeamsService, TeamsService>();

            //Configure Repositories
            builder.Services.AddScoped<IMatchUpsRepository, MatchUpsRepository>();
            builder.Services.AddScoped<ITeamsRepository, TeamsRepository>();

            builder.Services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseCors(options => options.AllowAnyOrigin());

            app.MapControllers();

            app.Run();
        }
    }
}