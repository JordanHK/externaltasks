﻿namespace StreetHoops.API.Utils
{
    public class StreetHoopsConstants
    {
        public const string serverName = ".";
        public const string dbName = "StreetHoops";

        public const string sqlConnectionString =
            $@"Server={serverName};Database={dbName};Trusted_Connection=True;";
    }
}