﻿namespace StreetHoops.DTO
{
    public class TeamDTO
    {
        public string Name { get; set; }
        public int? Score { get; set; }
    }
}