# StreetHoops

## Project Description

Simple basketball statistics application

# Deployment - Local Run

The Backend and Frontend parts of the application run separetely.

Backend Requirements: Visual Studio 2022 with .NET Core 6.0.

Open the `StreetHoops.sln` file and make sure to set  `StreetHoops.API` as a startup poject. From the Package Manager Console set the Default project to `StreetHoops.Data`. 

Run the following command:

```bash
  update-database
```

Run BalkanStone.API using IIS Express (port 65013).

Swagger documentation will open in a browser tab automatically.

# Angular 15 (Front-End)

In VSCode, navigate to StreetHoops/StreetHoops.UI folder. Once there, you will need to open the terminal and install all project dependencies using the `npm install` command.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 

## Author

- [Jordan Kalenitzov](https://gitlab.com/JordanHK)
