﻿using Microsoft.EntityFrameworkCore;
using StreetHoops.Data;
using StreetHoops.Models;
using StreetHoops.Repositories.Contracts;

namespace StreetHoops.Repositories
{
    public class TeamsRepository : ITeamsRepository
    {
        private readonly StreetHoopsContext context;

        private const string spAllTeams = "spGetAllTeams"; 
        private const string spOffensiveTeams = "spGetTopOffensiveTeams";
        private const string spGetTeam = "spGetTeamById {0}";
        private const string spDefensiveTeams = "spGetTopDefensiveTeams";

        public TeamsRepository(StreetHoopsContext context)
        {
            this.context = context;
        }

        public async Task<ICollection<Team>> GetTopOffensive()
        {
            var teams =
                await this.context.Teams.FromSqlRaw(spOffensiveTeams).ToListAsync()
                ?? null;

            return teams;
        }

        public async Task<ICollection<Team>> GetTopDefensive()
        {
            var teams =
                await this.context.Teams.FromSqlRaw(spDefensiveTeams).ToListAsync()
                ?? null;

            return teams;
        }

        public async Task<Team> GetTeam(int id)
        {
            var procedure = String.Format(spGetTeam, id);
            var team =
                await this.context.Teams
                    .FromSqlRaw(procedure)
                    .ToListAsync() ?? null;

            if (team != null)
            {
                return team.FirstOrDefault();
            }

            return null;
        }

        public async Task<ICollection<Team>> GetAll()
        {
            var teams = await this.context.Teams.FromSqlRaw(spAllTeams).ToListAsync() ?? null;

            return teams;
        }
    }
}