﻿using Microsoft.EntityFrameworkCore;
using StreetHoops.Data;
using StreetHoops.Models;
using StreetHoops.Repositories.Contracts;

namespace StreetHoops.Repositories
{
    public class MatchUpsRepository : IMatchUpsRepository
    {
        private readonly StreetHoopsContext context;

        private const string spAllMatchUps = "spGetAllMatchUps";
        private const string spHighlightMatchUp = "spHighlightMatchUp";


        public MatchUpsRepository(StreetHoopsContext context)
        {
            this.context = context;
        }

        public async Task<ICollection<MatchUp>> GetAll()
        {
            var matchUps =
                await this.context.Matches.FromSqlRaw(spAllMatchUps).ToListAsync() ?? null;

            return matchUps;
        }

        public async Task<ICollection<MatchUp>> GetHighlight()
        {
            var matchUps =
                await this.context.Matches.FromSqlRaw(spHighlightMatchUp).ToListAsync()
                ?? null;

            return matchUps;
        }
    }
}