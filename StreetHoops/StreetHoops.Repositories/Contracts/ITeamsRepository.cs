﻿using StreetHoops.Models;

namespace StreetHoops.Repositories.Contracts
{
    public interface ITeamsRepository
    {
        public Task<ICollection<Team>> GetAll();

        public Task<ICollection<Team>> GetTopOffensive();

        public Task<ICollection<Team>> GetTopDefensive();

        public Task<Team> GetTeam(int id);
    }
}