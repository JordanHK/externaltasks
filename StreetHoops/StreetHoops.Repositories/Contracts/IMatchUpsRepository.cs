﻿using StreetHoops.Models;

namespace StreetHoops.Repositories.Contracts
{
    public interface IMatchUpsRepository
    {
        public Task<ICollection<MatchUp>> GetAll();

        public Task<ICollection<MatchUp>> GetHighlight();
    }
}