import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { GlobalErrorPageComponent } from './pages/global-error-page/global-error-page.component';
import { MatchupsComponent } from './pages/matchUps/matchups.component';
import { NotFoundComponent } from './pages/notfound/not-found.component';
import { TeamsComponent } from './pages/teams/teams.component';

const routes: Routes = [
  {path: 'error', component: GlobalErrorPageComponent},
  { path: '', children: [{ path: 'index', component: NavComponent }] },
  {
    path: 'matches',
    children: [
      {
        path: 'all',
        component: MatchupsComponent,
        data: { viewOption: 'all' },
      },
      {
        path: 'highlight',
        component: MatchupsComponent,
        data: { viewOption: 'highlight' },
      },
    ],
  },
  {
    path: 'teams',
    children: [
      { path: 'all', component: TeamsComponent, data: { viewOption: 'all' } },
      {
        path: 'topoffensive',
        component: TeamsComponent,
        data: { viewOption: 'topoffensive' },
      },
      {
        path: 'topdefensive',
        component: TeamsComponent,
        data: { viewOption: 'topdefensive' },
      },
    ],
  },
  { path: '**', pathMatch: 'full', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
