import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Matchup } from 'src/app/pages/matchUps/model/matchup.model';
import { MatchupsService } from './service/matchups.service';

@Component({
  selector: 'app-matchups',
  templateUrl: './matchups.component.html',
  styleUrls: ['./matchups.component.css'],
})
export class MatchupsComponent {
  constructor(
    private matchupsService: MatchupsService,
    private activatedRoute: ActivatedRoute
  ) {}

  matchUpsList: Matchup[] = [];
  viewOption: String = '';
  tableTitle: String = '';

  ngOnInit() {
    this.activatedRoute.data.subscribe((data) => {
      this.viewOption = data['viewOption'];
    });

    this.viewResult();
  }

  viewResult() {
    if (this.viewOption.endsWith('all')) {
      this.matchupsService
        .getAllMatchUps()
        .pipe()
        .subscribe((matchups) => {
          this.matchUpsList = matchups;
        });

      this.tableTitle = 'All matchups';
    } else if (this.viewOption.endsWith('highlight')) {
      this.matchupsService
        .getHighlightMatchUp()
        .pipe()
        .subscribe((matchups) => {
          this.matchUpsList = matchups;
        });

      this.tableTitle = 'Highlight matchup';
    }
  }
}
