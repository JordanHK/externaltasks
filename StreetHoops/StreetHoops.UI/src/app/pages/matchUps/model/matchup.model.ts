export class Matchup {
  constructor(
    public homeTeam: string = '',
    public awayTeam: string = '',
    public homeTeamScore: number = 0,
    public awayTeamScore: number = 0
  ) {}
}
