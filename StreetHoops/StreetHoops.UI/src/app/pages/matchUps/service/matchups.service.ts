import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Matchup } from 'src/app/pages/matchUps/model/matchup.model';

@Injectable({
  providedIn: 'root',
})
export class MatchupsService {
  constructor(private http: HttpClient) {}

  matchUps: Matchup[] = [];

  private apiPathAll = 'http://localhost:65013/api/matches/all';
  private apiPathHighlight = 'http://localhost:65013/api/matches/highlight';

  getAllMatchUps(): Observable<any> {
    return this.http.get(this.apiPathAll);
  }

  getHighlightMatchUp(): Observable<any> {
    return this.http.get(this.apiPathHighlight);
  }
}
