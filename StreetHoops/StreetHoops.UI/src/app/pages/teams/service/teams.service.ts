import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from '../model/team.model';

@Injectable({
  providedIn: 'root',
})
export class TeamsService {
  teams: Team[] = [];

  private apiPathAll = 'http://localhost:65013/api/teams/all';
  private apiPathTopOffensive = 'http://localhost:65013/api/teams/topoffensive';
  private apiPathToDefensive = 'http://localhost:65013/api/teams/topdefensive';

  constructor(private http: HttpClient) {}

  getAllTeams(): Observable<any> {
    return this.http.get(this.apiPathAll);
  }

  getTopOffensive(): Observable<any> {
    return this.http.get(this.apiPathTopOffensive);
  }

  getTopDefensive(): Observable<any> {
    return this.http.get<Team[]>(this.apiPathToDefensive);
  }
}
