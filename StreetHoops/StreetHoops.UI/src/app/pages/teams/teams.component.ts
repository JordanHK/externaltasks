import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Team } from './model/team.model';
import { TeamsService } from './service/teams.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css'],
})
export class TeamsComponent {
  constructor(
    private teamsService: TeamsService,
    private activatedRoute: ActivatedRoute
  ) {}

  teamsList: Team[] = [];
  viewOption: String = '';
  tableTitle: String = '';

  sortColumn: String = 'Team';
  reverse: Boolean = false;
  searchTerm: String = '';

  ngOnInit() {
    this.activatedRoute.data.subscribe((data) => {
      this.viewOption = data['viewOption'];
    });

    this.viewResult();
  }

  viewResult() {
    if (this.viewOption.endsWith('all')) {
      this.teamsService
        .getAllTeams()
        .pipe()
        .subscribe((teams) => {
          this.teamsList = teams;
        });

      this.tableTitle = 'All teams';
    } else if (this.viewOption.endsWith('topoffensive')) {
      this.teamsService
        .getTopOffensive()
        .pipe()
        .subscribe((teams) => {
          this.teamsList = teams;
        });

      this.tableTitle = 'Top 5 offensive teams';
    } else if (this.viewOption.endsWith('topdefensive')) {
      this.teamsService
        .getTopDefensive()
        .pipe()
        .subscribe((teams) => {
          this.teamsList = teams;
        });

      this.tableTitle = 'Top 5 defensive teams';
    }
  }

  sort(column: String) {
    if (this.sortColumn === column) {
      this.reverse = !this.reverse;
    }

    this.sortColumn = column;
  }
}
