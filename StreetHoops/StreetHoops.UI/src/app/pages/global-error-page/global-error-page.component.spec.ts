import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalErrorPageComponent } from './global-error-page.component';

describe('GlobalErrorPageComponent', () => {
  let component: GlobalErrorPageComponent;
  let fixture: ComponentFixture<GlobalErrorPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GlobalErrorPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GlobalErrorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
