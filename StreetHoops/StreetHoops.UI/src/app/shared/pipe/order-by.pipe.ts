import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {
  transform(arr: any[], column?: String, reverse?: Boolean): any[] {
    if (column === undefined || column === 'name') {
      arr.sort((t1, t2) => t1.name.localeCompare(t2.name));
    } else if (column === 'score') {
      arr.sort((t1, t2) => t1.score - t2.score);
    }
    if (reverse) {
      arr.reverse();
    }
    return arr;
  }
}
