import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(arr: any, args?: any): any {
    if(!args){
      return arr;
    }
    return arr.filter((obj: { name: string; }) => {
      return obj.name.toLocaleLowerCase().includes(args);
    })
  }

}
