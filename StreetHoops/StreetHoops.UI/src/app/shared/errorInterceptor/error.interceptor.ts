import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  errorUrl: string = "/error";
  constructor(private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
    .pipe(catchError((error: HttpErrorResponse) => {
      let errorMessage = "";
      if (error.error instanceof ErrorEvent) {
          errorMessage = `Error: ${error.error.message}`;
      }
      else{
        errorMessage = `Error Status: ${error.status}\nMessage: ${error.message}`;
      }
      console.log(errorMessage);      
      this.router.navigate([this.errorUrl]);
      return throwError(() => new Error(errorMessage));
    }))
  }
}
