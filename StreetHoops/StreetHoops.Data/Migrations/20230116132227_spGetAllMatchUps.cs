﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StreetHoops.Data.Migrations
{
    public partial class spGetAllMatchUps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string spGetAllMatchUps =
                @"CREATE PROCEDURE spGetAllMatchUps
                AS
                BEGIN
                    SELECT m.Id, t.Id as HomeTeamId, t1.id as AwayTeamId, m.HomeTeamScore, m.AwayTeamScore 
                    FROM Matches AS m
                    JOIN Teams AS t ON m.HomeTeamId = t.Id
                    JOIN Teams AS t1 ON m.AwayTeamId = t1.Id
                END";

            migrationBuilder.Sql(spGetAllMatchUps);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            string spGetAllMatchUps = @"DROP PROCEDURE spGetAllMatchUps";

            migrationBuilder.Sql(spGetAllMatchUps);
        }
    }
}
