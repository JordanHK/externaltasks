﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StreetHoops.Data.Migrations
{
    public partial class spGetMostEfficientMatchUp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string spHighlightMatchUp =
                @"CREATE PROCEDURE spHighlightMatchUp
                AS
                BEGIN
                    SELECT TOP(1) m.Id, t.id as HomeTeamId, t1.id as AwayTeamId, m.HomeTeamScore, m.AwayTeamScore
                    FROM Matches AS m
                    	JOIN Teams AS t ON m.HomeTeamId = t.Id
                    	JOIN Teams AS t1 ON m.AwayTeamId = t1.Id
                    ORDER BY (m.HomeTeamScore + m.AwayTeamScore) DESC
                END";

            migrationBuilder.Sql(spHighlightMatchUp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            string spHighlightMatchUp = @"DROP PROCEDURE spHighlightMatchUp";

            migrationBuilder.Sql(spHighlightMatchUp);
        }
    }
}
