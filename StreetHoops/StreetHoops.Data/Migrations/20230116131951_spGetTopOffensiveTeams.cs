﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StreetHoops.Data.Migrations
{
    public partial class spGetTopOffensiveTeams : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string spGetTopOffensiveTeams =
                @"CREATE PROCEDURE spGetTopOffensiveTeams
                AS
                BEGIN
                	SELECT Teams.Id, Teams.Name, s as Score
                	FROM(
                		SELECT TOP(5) TeamId, SUM(s) AS s
                		FROM(
                			SELECT HomeTeamId AS TeamId, SUM(HomeTeamScore) AS s
                			FROM Matches
                			GROUP BY HomeTeamId
                		Union
                			SELECT AwayTeamId AS TeamId, SUM(AwayTeamScore) AS s
                			FROM Matches
                			GROUP BY AwayTeamId) Offensive
                	GROUP BY TeamId
                	ORDER BY s DESC) Totals
                	JOIN Teams ON Teams.Id = TeamId
                END";

            migrationBuilder.Sql(spGetTopOffensiveTeams);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            string spGetTopOffensiveTeams = @"DROP PROCEDURE spGetTopOffensiveTeams";

            migrationBuilder.Sql(spGetTopOffensiveTeams);
        }
    }
}
