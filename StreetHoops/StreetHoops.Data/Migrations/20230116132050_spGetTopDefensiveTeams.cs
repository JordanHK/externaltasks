﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StreetHoops.Data.Migrations
{
    public partial class spGetTopDefensiveTeams : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string spGetTopDefensiveTeams =
                @"CREATE PROCEDURE spGetTopDefensiveTeams
                AS
                BEGIN
                    SELECT Teams.id, Teams.Name, s as Score
                    FROM(
                    	SELECT TOP(5) TeamId, SUM(s) AS s
                    	FROM(
                    		SELECT HomeTeamId AS TeamId, SUM(AwayTeamScore) AS s
                    		FROM Matches
                    		GROUP BY HomeTeamId
                    	Union
                    		SELECT AwayTeamId AS TeamId, SUM(HomeTeamScore) AS s
                    		FROM Matches
                    		GROUP BY AwayTeamId) Defensive
                    	GROUP BY TeamId
                    	ORDER BY s) Totals
                    JOIN Teams ON Teams.Id = TeamId
                END";

            migrationBuilder.Sql(spGetTopDefensiveTeams);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            string spGetTopDefensiveTeams = @"DROP PROCEDURE spGetTopDefensiveTeams";

            migrationBuilder.Sql(spGetTopDefensiveTeams);
        }
    }
}
