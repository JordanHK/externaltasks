﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StreetHoops.Data.Migrations
{
    public partial class spGetAllTeams : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string spGetAllTeams =
                    @"CREATE PROCEDURE spGetAllTeams
                    AS
                    BEGIN
                	SELECT Teams.Id, Teams.Name, s as Score
                	FROM(
                		SELECT TeamId, SUM(s) AS s
                		FROM(
                			SELECT HomeTeamId AS TeamId, SUM(HomeTeamScore) AS s
                			FROM Matches
                			GROUP BY HomeTeamId
                		Union
                			SELECT AwayTeamId AS TeamId, SUM(AwayTeamScore) AS s
                			FROM Matches
                			GROUP BY AwayTeamId) Offensive
                	GROUP BY TeamId) Totals
                	JOIN Teams ON Teams.Id = TeamId
                    END";

            migrationBuilder.Sql(spGetAllTeams);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            string spGetAllTeams = @"DROP PROCEDURE spGetAllTeams";

            migrationBuilder.Sql(spGetAllTeams);
        }
    }
}
