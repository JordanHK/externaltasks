﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StreetHoops.Data.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Score = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Matches",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HomeTeamId = table.Column<int>(type: "int", nullable: false),
                    AwayTeamId = table.Column<int>(type: "int", nullable: false),
                    HomeTeamScore = table.Column<int>(type: "int", nullable: false),
                    AwayTeamScore = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Matches", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Matches_Teams_AwayTeamId",
                        column: x => x.AwayTeamId,
                        principalTable: "Teams",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Matches_Teams_HomeTeamId",
                        column: x => x.HomeTeamId,
                        principalTable: "Teams",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Name", "Score" },
                values: new object[,]
                {
                    { 1, "Chicago Bulls", null },
                    { 2, "Utah Jazz", null },
                    { 3, "Boston Celtics", null },
                    { 4, "Memphis Grizzlies", null },
                    { 5, "Philadelphia 76ers", null },
                    { 6, "Orlando Magic", null }
                });

            migrationBuilder.InsertData(
                table: "Matches",
                columns: new[] { "Id", "AwayTeamId", "AwayTeamScore", "HomeTeamId", "HomeTeamScore" },
                values: new object[,]
                {
                    { 1, 4, 35, 3, 47 },
                    { 2, 2, 27, 6, 19 },
                    { 3, 4, 45, 2, 93 },
                    { 4, 2, 67, 6, 14 },
                    { 5, 5, 103, 2, 91 },
                    { 6, 3, 8, 5, 15 },
                    { 7, 3, 35, 1, 21 },
                    { 8, 6, 70, 1, 120 },
                    { 9, 3, 83, 4, 30 },
                    { 10, 3, 63, 5, 32 },
                    { 11, 5, 5, 2, 72 },
                    { 12, 6, 90, 3, 1 },
                    { 13, 5, 116, 2, 51 },
                    { 14, 2, 104, 5, 114 },
                    { 15, 1, 26, 5, 104 },
                    { 16, 1, 92, 4, 36 },
                    { 17, 6, 94, 1, 114 },
                    { 18, 6, 104, 4, 76 },
                    { 19, 6, 106, 1, 64 },
                    { 20, 1, 76, 3, 19 },
                    { 21, 3, 43, 5, 110 },
                    { 22, 4, 61, 6, 22 },
                    { 23, 6, 113, 5, 99 },
                    { 24, 4, 70, 6, 43 },
                    { 25, 6, 69, 3, 3 },
                    { 26, 2, 97, 4, 57 },
                    { 27, 1, 12, 3, 107 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Matches_AwayTeamId",
                table: "Matches",
                column: "AwayTeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Matches_HomeTeamId",
                table: "Matches",
                column: "HomeTeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Matches");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
