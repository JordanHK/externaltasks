﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StreetHoops.Data.Migrations
{
    public partial class spGetTeamById : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string spGetTeamById =
                    @"CREATE PROCEDURE spGetTeamById
                    @Id int
                    AS
                    BEGIN
                    	SELECT * FROM Teams
                    	WHERE Id = @Id
                    END";

            migrationBuilder.Sql(spGetTeamById);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            string spGetTeamById = @"DROP PROCEDURE spGetTeamById";

            migrationBuilder.Sql(spGetTeamById);
        }
    }
}
