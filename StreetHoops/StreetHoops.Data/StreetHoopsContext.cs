﻿using Microsoft.EntityFrameworkCore;
using StreetHoops.Models;

namespace StreetHoops.Data
{
    public class StreetHoopsContext : DbContext
    {
        public StreetHoopsContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Team> Teams { get; set; }
        public DbSet<MatchUp> Matches { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configure relationships
            modelBuilder
                .Entity<MatchUp>()
                .HasOne(m => m.HomeTeam)
                .WithMany(t => t.HomeMatches)
                .HasForeignKey(m => m.HomeTeamId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder
                .Entity<MatchUp>()
                .HasOne(m => m.AwayTeam)
                .WithMany(t => t.AwayMatches)
                .HasForeignKey(m => m.AwayTeamId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            // Seed data
            modelBuilder.Entity<Team>().HasData(DataSeeder.Teams);
            modelBuilder.Entity<MatchUp>().HasData(DataSeeder.Matches);
        }
    }
}