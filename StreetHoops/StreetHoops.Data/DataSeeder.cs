﻿using StreetHoops.Models;

namespace StreetHoops.Data
{
    public class DataSeeder
    {
        public static List<Team> Teams =>
            new List<Team>
            {
                new Team { Id = 1, Name = "Chicago Bulls" },
                new Team { Id = 2, Name = "Utah Jazz" },
                new Team { Id = 3, Name = "Boston Celtics" },
                new Team { Id = 4, Name = "Memphis Grizzlies" },
                new Team { Id = 5, Name = "Philadelphia 76ers" },
                new Team { Id = 6, Name = "Orlando Magic" }
            };

        public static List<MatchUp> Matches =>
            new List<MatchUp>
            {
                new MatchUp
                {
                    Id = 1,
                    HomeTeamId = 3,
                    AwayTeamId = 4,
                    HomeTeamScore = 47,
                    AwayTeamScore = 35
                },
                new MatchUp
                {
                    Id = 2,
                    HomeTeamId = 6,
                    AwayTeamId = 2,
                    HomeTeamScore = 19,
                    AwayTeamScore = 27
                },
                new MatchUp
                {
                    Id = 3,
                    HomeTeamId = 2,
                    AwayTeamId = 4,
                    HomeTeamScore = 93,
                    AwayTeamScore = 45
                },
                new MatchUp
                {
                    Id = 4,
                    HomeTeamId = 6,
                    AwayTeamId = 2,
                    HomeTeamScore = 14,
                    AwayTeamScore = 67
                },
                new MatchUp
                {
                    Id = 5,
                    HomeTeamId = 2,
                    AwayTeamId = 5,
                    HomeTeamScore = 91,
                    AwayTeamScore = 103
                },
                new MatchUp
                {
                    Id = 6,
                    HomeTeamId = 5,
                    AwayTeamId = 3,
                    HomeTeamScore = 15,
                    AwayTeamScore = 8
                },
                new MatchUp
                {
                    Id = 7,
                    HomeTeamId = 1,
                    AwayTeamId = 3,
                    HomeTeamScore = 21,
                    AwayTeamScore = 35
                },
                new MatchUp
                {
                    Id = 8,
                    HomeTeamId = 1,
                    AwayTeamId = 6,
                    HomeTeamScore = 120,
                    AwayTeamScore = 70
                },
                new MatchUp
                {
                    Id = 9,
                    HomeTeamId = 4,
                    AwayTeamId = 3,
                    HomeTeamScore = 30,
                    AwayTeamScore = 83
                },
                new MatchUp
                {
                    Id = 10,
                    HomeTeamId = 5,
                    AwayTeamId = 3,
                    HomeTeamScore = 32,
                    AwayTeamScore = 63
                },
                new MatchUp
                {
                    Id = 11,
                    HomeTeamId = 2,
                    AwayTeamId = 5,
                    HomeTeamScore = 72,
                    AwayTeamScore = 5
                },
                new MatchUp
                {
                    Id = 12,
                    HomeTeamId = 3,
                    AwayTeamId = 6,
                    HomeTeamScore = 1,
                    AwayTeamScore = 90
                },
                new MatchUp
                {
                    Id = 13,
                    HomeTeamId = 2,
                    AwayTeamId = 5,
                    HomeTeamScore = 51,
                    AwayTeamScore = 116
                },
                new MatchUp
                {
                    Id = 14,
                    HomeTeamId = 5,
                    AwayTeamId = 2,
                    HomeTeamScore = 114,
                    AwayTeamScore = 104
                },
                new MatchUp
                {
                    Id = 15,
                    HomeTeamId = 5,
                    AwayTeamId = 1,
                    HomeTeamScore = 104,
                    AwayTeamScore = 26
                },
                new MatchUp
                {
                    Id = 16,
                    HomeTeamId = 4,
                    AwayTeamId = 1,
                    HomeTeamScore = 36,
                    AwayTeamScore = 92
                },
                new MatchUp
                {
                    Id = 17,
                    HomeTeamId = 1,
                    AwayTeamId = 6,
                    HomeTeamScore = 114,
                    AwayTeamScore = 94
                },
                new MatchUp
                {
                    Id = 18,
                    HomeTeamId = 4,
                    AwayTeamId = 6,
                    HomeTeamScore = 76,
                    AwayTeamScore = 104
                },
                new MatchUp
                {
                    Id = 19,
                    HomeTeamId = 1,
                    AwayTeamId = 6,
                    HomeTeamScore = 64,
                    AwayTeamScore = 106
                },
                new MatchUp
                {
                    Id = 20,
                    HomeTeamId = 3,
                    AwayTeamId = 1,
                    HomeTeamScore = 19,
                    AwayTeamScore = 76
                },
                new MatchUp
                {
                    Id = 21,
                    HomeTeamId = 5,
                    AwayTeamId = 3,
                    HomeTeamScore = 110,
                    AwayTeamScore = 43
                },
                new MatchUp
                {
                    Id = 22,
                    HomeTeamId = 6,
                    AwayTeamId = 4,
                    HomeTeamScore = 22,
                    AwayTeamScore = 61
                },
                new MatchUp
                {
                    Id = 23,
                    HomeTeamId = 5,
                    AwayTeamId = 6,
                    HomeTeamScore = 99,
                    AwayTeamScore = 113
                },
                new MatchUp
                {
                    Id = 24,
                    HomeTeamId = 6,
                    AwayTeamId = 4,
                    HomeTeamScore = 43,
                    AwayTeamScore = 70
                },
                new MatchUp
                {
                    Id = 25,
                    HomeTeamId = 3,
                    AwayTeamId = 6,
                    HomeTeamScore = 3,
                    AwayTeamScore = 69
                },
                new MatchUp
                {
                    Id = 26,
                    HomeTeamId = 4,
                    AwayTeamId = 2,
                    HomeTeamScore = 57,
                    AwayTeamScore = 97
                },
                new MatchUp
                {
                    Id = 27,
                    HomeTeamId = 3,
                    AwayTeamId = 1,
                    HomeTeamScore = 107,
                    AwayTeamScore = 12
                }
            };
    }
}